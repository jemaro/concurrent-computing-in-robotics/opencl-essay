\section{Introduction}
\label{sec:introduction}
This work summarize the most important information and key learnings from the
author's first steps in OpenCL programming as part of the JEMARO course
Concurrent Computing in Robotics. The associated code can be found in the
\href{\repo}{code repository}. In this section we will introduce OpenCL as well
as some important concepts and terminology. \Cref{sec:overview} describes an
example application of OpenCL: a matrix multiplication. \Cref{sec:evaluation}
discusses the results of the application, some performance tweaks and results
of tunning the hardware configuration. Finally, \cref{sec:conclusion}
summarizes the learnings of the author's first steps in OpenCL programming.

OpenCL supports a wide range of applications, ranging from embedded and
consumer software to HPC (High Performance Computing) solutions
\cite{OpenCLSpec}. This can only be done by portable abstraction. Any
application will be considered the combination of the program running on the
\emph{host} and OpenCL \emph{devices}. First of all, it is important to define
a set of important concepts: \emph{host}, \emph{device} and \emph{platform}.
Then we will be able to approach the topic of \nameref{subsec:programs}. We
will finish this section with some remarks on
\nameref{subsec:heterogeneous-computing}.

\simplefigure{platform-model.png}{Platform Model: A \emph{host} connected to
one or more \emph{devices}}

\paragraph{Host} 
portion of the application that runs on a host processor according to the
models native to the host platform (i.e., a laptop with a Windows operating
system). The \emph{host} code submits \emph{commands} to the \emph{devices} it
is connected to. It can be programmed in C, using the OpenCL C API, or in many
other languages where the OpenCL API bindings are available (like
\href{https://pypi.org/project/pyopencl/}{Python}). One can see an example of
basic host C++ code in the author's
\href{\repoblob/tests/test_matmul.cpp#L109}{code repository}. The OpenCL API
defines a \emph{context} which is the environment used by the \emph{host} to
interact with \emph{devices}. It includes a set of \emph{devices}, the memory
accessible to those \emph{devices}, the corresponding memory properties and one
or more \emph{command queues} used to schedule execution of a \emph{kernel} or
operations on memory objects.

\paragraph{Device} 
is a collection of \emph{compute units}, which can be further divided into one
or more \emph{processing elements}, as shown in \cref{fig:platform-model}.
Computations on a device occur within the \emph{processing elements}. A
\emph{command-queue} is used to queue \emph{commands} to a \emph{device}.
Examples of \emph{commands} include executing \emph{kernels}, or reading and
writing \emph{memory objects}. OpenCL devices typically correspond to a GPU, a
multi-core CPU, and other processors such as DSPs. Don't think that a
\emph{processing element} is a "Processor" or CPU Core. You don't need to think
on how the OpenCL device model fit on a specific hardware, this is the
responsibility of the hardware vendor: The \emph{platform}.

\paragraph{Platform} 
a specific OpenCL implementation. Hardware vendors usually develop an OpenCL
implementation for their \emph{devices}. A single \emph{host} can interact with
multiple platforms making use of different \emph{contexts}. A single
\emph{context} can interact with multiple \emph{devices} as long as they are
from the same \emph{platform} (i.e., "Intel(R) OpenCL" or "NVIDIA CUDA"). A
\emph{platform} provides a compiler to translate \emph{programs} into
executable objects for a certain \emph{device}. The Installable Client Driver
(ICD) is the mechanism that allows different OpenCL implementations
(\emph{platforms}) from multiple vendors to coexist on a host system. The
\href{https://github.com/KhronosGroup/OpenCL-ICD-Loader}{OpenCL ICD Loader
library} allows applications to choose a platform from the list of installed
platforms and dispatches OpenCL API calls to the underlying implementations.

\subsection{Programs}
\label{subsec:programs}
Up to now, we have differentiated between \emph{host} and \emph{devices}. They
run different programs. We know that the \emph{host} is programmed with common
programming languages (C, C++, Python, etc.). However, the \emph{device}
program is usually written in a specific subset of C, called \emph{OpenCL C}.
This code needs to be compiled for a specific \emph{device}.

The OpenCL compiler may build program executables from OpenCL C source strings,
the SPIR-V intermediate language, or device-specific program binary objects,
depending on the capabilities of a device. Other kernel languages or
intermediate languages may be supported by some implementations.
\href{https://www.khronos.org/registry/OpenCL/specs/3.0-unified/html/OpenCL_API.html#CL_PLATFORM_PROFILE}{Full
profile platforms} provide an \emph{online} compiler, which is available during
the \emph{host} program execution using the OpenCL API. This means that the
\emph{host} program can adapt to the available devices at runtime.
\emph{Device} code can be compiled also offline and loaded at \emph{host}
runtime, but that is a more advanced topic with a tradeoff between runtime
performance, code size and portability.

% https://github.com/KhronosGroup/OpenCL-ICD-Loader

\subsection{Heterogeneous computing}
\label{subsec:heterogeneous-computing}
Modern computing systems are heterogeneous. Any laptop will be composed from at
least a CPU and a GPU. OpenCL gives tools for developers to embrace
heterogeneous computing and improve the performance of a task by executing it
on the best fitted hardware.

The very base of OpenCL is an abstraction of the hardware into \emph{devices}.
Developers should not be concerned with the hardware details. The task could be
solved with any \emph{device} because the \emph{kernel} code is portable. The
\emph{host} can be programmed in a way that it schedules different
\emph{kernels} (solutions for a certain task) to different devices depending on
its availability. OpenCL based software can squeeze the host machine for the
best performance and the user wouldn't even notice. This potential comes with a
complexity though. Hardware abstraction makes it difficult to tune the usage of
a specific hardware (let's say a GPU) to optimize the performance. OpenCL seeks
to increase the performance by tunning the usage of the whole system, while
other frameworks like CUDA or OpenMP are designed to increase the performance
by tunning the usage of a specific hardware.

