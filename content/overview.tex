\section{Overview}
\label{sec:overview}

In this section we walk through a sample application for an OpenCL task: A
matrix multiplication. 

\paragraph{Definition}
If $A$ is an $m \times n$ matrix and $B$ is an $n \times p$ matrix,

$$
\mathbf{A}=\left(\begin{array}{cccc}
a_{11} & a_{12} & \cdots & a_{1 n} \\
a_{21} & a_{22} & \cdots & a_{2 n} \\
\vdots & \vdots & \ddots & \vdots \\
a_{m 1} & a_{m 2} & \cdots & a_{m n}
\end{array}\right)
$$

$$
\mathbf{B}=\left(\begin{array}{cccc}
b_{11} & b_{12} & \cdots & b_{1 p} \\
b_{21} & b_{22} & \cdots & b_{2 p} \\
\vdots & \vdots & \ddots & \vdots \\
b_{n 1} & b_{n 2} & \cdots & b_{n p}
\end{array}\right)
$$

the \emph{matrix product} $C = AB$ is defined to be the $m \times p$ matrix

$$
\mathbf{C}=\left(\begin{array}{cccc}
c_{11} & c_{12} & \cdots & c_{1 p} \\
c_{21} & c_{22} & \cdots & c_{2 p} \\
\vdots & \vdots & \ddots & \vdots \\
c_{m 1} & c_{m 2} & \cdots & c_{m p}
\end{array}\right)
$$

such that

$$
c_{i j}=a_{i 1} b_{1 j}+a_{i 2} b_{2 j}+\cdots+a_{i n} b_{n j}=\sum_{k=1}^{n} a_{i k} b_{k j}
$$

for $i=1,\ldots,m$ and $j=1,\ldots,p$.

This can be directly represented in C/C++ code as in as \cref{lst:matmulC}.
Although this is far from the most efficient algorithm for matrix
multiplication.

\begin{lstlisting}[
    language=C, caption={Naive matrix multiplication implementation in C}, 
    label={lst:matmulC}
    ]
for (int i = 0; i < m; i++) {
  for (int j = 0; j < p; j++) {
    float c_ij = 0.0f;
    for (int k = 0; k < n; k++) {
      c_ij += A[i * n + k] * B[k * p + j];
    }
    C[i * p + j] = c_ij;
  }
}
\end{lstlisting}

In OpenCL C the implementation is similar as one can see in
\cref{lst:matmulOpenCL}, the difference is that we avoid the outer \texttt{for}
loops. Instead, each \emph{Processing Element} will compute a single element of
the result matrix $C$.

\begin{lstlisting}[
    language=C, caption={Naive matrix multiplication kernel in OpenCL C}, 
    label={lst:matmulOpenCL}
    ]
__kernel void matmul(
  __global float* C, 
  int p, 
  int n, 
  __global float* A, 
  __global float* B  
  ) {
  int j      = get_global_id(0);
  int i      = get_global_id(1);
  float c_ij = 0.0f;
  for (int k = 0; k < n; k++) {
    c_ij += A[i * n + k] * B[k * p + j];
  }
  C[i * p + j] = c_ij;
}
\end{lstlisting}

The \emph{host} program implementation can be found in the
\href{\repotree/src/opencl-example}{code repository}, together with
\href{\repoblob/tests/test_matmul.cpp}{tests} that assert the correctness of
the implementation.