\section{Evaluation}
\label{sec:evaluation}

The matrix multiplication implementation described in \cref{sec:overview} was
used in several benchmarks that asses the performance change with various
parameters. The source code used for the benchmarks can be again found in the
\href{\repoblob/benchmark/bm_matmul.cpp}{code repository}. The
\href{\repoblob/benchmark/results.json}{results} from the benchmark have been
analyzed and plotted using a \href{\repoblob/scripts/plot.ipynb}{Python
notebook}.

\subsubsection{Devices}
\label{subsec:eval-devices}

\Cref{fig:time-work-linear} demonstrates that even our naive matrix
multiplication implementation can outperform a state-of-the-art linear algebra
library when the matrix size is big enough. This reference implementation comes
from the C++ library \href{https://eigen.tuxfamily.org}{Eigen}. It also
showcases that GPUs are better suited for the matrix multiplication task than
CPUs.

Although \cref{fig:time-work-linear} can be misleading about the performance
with smaller workloads. \Cref{fig:time-work-log} shows that Eigen's
implementation is, as we expected, several orders of magnitude faster than our
naive implementation for small work sizes. This is due to the fact that their
algorithm is most probably way more efficient than ours. Nevertheless, this is
a good example of the potential of parallelization in heavy computing tasks.
\Cref{subsec:better-algorithms} discusses how our naive implementation could be
improved and which performance speedup could we expect with an optimized
algorithm.

\begin{figure}[h]
    \begin{subfigure}{.45\textwidth}
        \includegraphics[width=\textwidth]{OpenCL-example/benchmark/time-work.png}
        \caption{Linear scale}
        \label{fig:time-work-linear}
    \end{subfigure}
    \hfill
    \begin{subfigure}{.45\textwidth}
        \includegraphics[width=\textwidth]{OpenCL-example/benchmark/time-work-log.png}
        \caption{Logarithmic scale}
        \label{fig:time-work-log}
    \end{subfigure}
    \caption{ Comparison of matrix multiplication execution time (in
        milliseconds) depending on the matrix product size (in number of
        elements) between Eigen's and our OpenCL naive matrix multiplication
        implementation. Our implementation is executed using different devices
        with the default configuration. }
    \label{fig:time-work}
\end{figure}

\subsubsection{Number of cores}
\label{subsec:number-of-cores}

% How does performance change with the number of cores?

\Cref{fig:number-of-cores} shows how does the performance change with the
number of computing units. The first thing that we can observe is that
increasing the number of computing units does not always improve the
performance. There is an optimal number of computing units for each problem
size. 

The number of computing units was specified in the OpenCL API using
\href{https://www.khronos.org/registry/OpenCL/specs/3.0-unified/html/OpenCL_API.html\#clCreateSubDevices}{\texttt{clCreateSubDevices}}.
Therefore the device is partitioned in a way that it only has access to a
limited number of computing units Results of work size higher than $2^{22}$
matrix error have a higher error due to the fact that those computations were
executed only once. For contrast, results with a  work size of $2^{18}$ matrix
elements are the average of 25 executions.


\begin{figure}[h]
    \begin{subfigure}{.45\textwidth}
        \includegraphics[width=\textwidth]{OpenCL-example/benchmark/d0-speed-units.png}
        \caption{Absolute speed in matrix elements per second}
        \label{fig:speed}
    \end{subfigure}
    \hfill
    \begin{subfigure}{.45\textwidth}
        \includegraphics[width=\textwidth]{OpenCL-example/benchmark/d0-speedup-units.png}
        \caption{Speedup relative to the speed of a single core}
        \label{fig:speedup}
    \end{subfigure}
    \caption{ Benchmark results for our naive matrix multiplication
    implementation running in a general purpose Intel CPU with 6 cores that can
    execute up to 12 threads simultaneously thanks to Hyper-Threading. The
    horizontal axis represents the number of computing units. Left side chart
    corresponds to an execution without partitioning the device with the
    default configuration. The vertical axis represents the work size as the
    number of matrix product elements. The color represents a) the speed in
    matrix elements per second and b) the speedup defined as the ratio of the
    speed with respect to the speed of an execution with the same work size but
    on a device partitioned in a way that it has only access to a single
    computing unit. The dotted black line represents the size of the device's
    global cache memory. }
    \label{fig:number-of-cores}
\end{figure}

% Change the problem size. Observe the difference in performance between the
% single threaded and OpenCL solutions as you increase the problem size.
% Observe the point at which increasing the problem size does not change the
% performance gain. Comment why.

\Cref{fig:speed} illustrates that the absolute computing speed generally
increases with the work size. Until it reaches a barrier where the absolute
computing speed drops. This barrier coincides with the device \emph{global
memory cache size}. Therefore the speed decrease is expected as the problem is
too big and it has to be stored in a slower memory tier.

It can be misleading to pay attention only to the speedup as it is shown in
\cref{fig:speedup}. One could have missed the performance loss with the
increase of the problem size. It is worth to mention that a different
algorithm could deal better these memory limitations.

We would also like to highlight the fact that OpenCL default configuration (as
shown in the left side of \cref{fig:number-of-cores}) is not the optimal
performance but it is not the worst either. OpenCL adapts by default to the
work size, although deep knowledge of both the task and the device hardware can
lead to a higher performance configuration.


\subsubsection{Better algorithms}
\label{subsec:better-algorithms}

The main reason the naive implementation doesn't perform so well is because we
are accessing the global memory way too often. Although our device cache is
probably helping a lot, we could get much more performance by manually caching
sub-blocks of the matrices (tiles) on the device local memory \cite{cnugteren}.

\Cref{fig:tiling} illustrates how in order to compute a tile of the output
matrix $C$, only a subset of tiles from the input matrices $A$ and $B$ are
needed. We can benefit from reusing memory by moving global memory to the
device local memory, which is faster to access. Due to the fact that all the
\emph{processing elements} within a \emph{computing unit} share the same local
memory, the different processing elements need to be synchronized in a way that
they do not load the data needed to process a tile before the previous tile has
been completely computed. We will not enter into the details of how to
implement it, as it is beyond the scope of this work, one can find
\href{https://cnugteren.github.io/tutorial/pages/page4.html}{example
implementations online}.

\simplefigure{tiling.png}{Tiling: To compute a sub-block of $C$
(\textcolor{Plum}{purple} tile), we need $A$'s corresponding rows
(\textcolor{Green}{green} tiles) and $B$'s corresponding columns
(\textcolor{NavyBlue}{blue} tiles).}

Our naive implementation was very simple and close to the corresponding pure
C++ code, with no synchronization mechanisms needed. But, as
\cref{fig:optimization-margin} shows, the added complexity of tiling can
triplicate the throughput of a naive implementation. One can also see that
further OpenCL optimizations could tenfold the throughput of a naive
implementation.

\simplefigure{optimization-margin.png}{ Example matrix multiplication
    throughput benchmark results with different OpenCL kernels. Compared also
    to industry libraries (cuBLAS and clBlas). Our naive implementation
    described in \cref{sec:overview} would be analogous to \emph{myGEMM1
    (naive)}. All tests were performed on a Kepler SM 3.5 GPU, the Tesla K40m.
    The GPU was configured with ECC enabled. Version 6.5 of the CUDA toolkit
    was used (including OpenCL).}